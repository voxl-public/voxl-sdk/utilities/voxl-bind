/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h> // memset

// project include
#include "voxl_bind.h"

// paths to configure GPIO pins
#define FILE_GPIO_EXPORT    "/sys/class/gpio/export"
#define FILE_GPIO_DIRECTION "/sys/class/gpio/gpio%d/direction"
#define FILE_GPIO_VALUE     "/sys/class/gpio/gpio%d/value"

// GPIO pin toggles
#define PIN_LOW                 0
#define PIN_HIGH                1

// GPIO direction toggles
#define GPIO_DIRECTION_OUTPUT   0
#define GPIO_DIRECTION_INPUT    1

// GPIO pins
#define GPIO_CHIP           1100
#define M0054_SPEKTRUM_POWER_PIN  159 + GPIO_CHIP
#define M0054_SPEKTRUM_BIND_PIN   46  + GPIO_CHIP // Note this pin has an inverted signal
#define M0154_SPEKTRUM_BIND_PIN   20  + GPIO_CHIP // Note this pin has an inverted signal

static int en_debug = 0;
static spektrum_bind_pulse pulse_type = UNDEFINED;


static void _print_usage(){
    printf("\n\
Tool used for binding Spektrum receiver. In order to use,\n\
select ONLY 1 bind type\n\
-d, --debug         enable debug messages\n\
-h, --help          print this help messsage\n\
-t, --DSM2_22       3 pulses (DSM2 1024/22ms)\n\
-f, --DSM2_11       5 pulses (DSM2 2048/11ms)\n\
-s, --DSMX_22       7 pulses (DSMX 22ms)\n\
\n");
}

// parse command line arguments
static int _parse_opts(int argc, char *argv[]){
    static struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"debug", no_argument, 0, 'd'},
        {"DSM2_22", no_argument, 0, 't'},
        {"DSM2_11", no_argument, 0, 'f'},
        {"DSMX_22", no_argument, 0, 's'},
        {0, 0, 0, 0}
    };

    while (1){
        int option_index = 0;
        int c = getopt_long(argc, argv, "dhtfsn", long_options, &option_index);

        // detect end of options
        if (c == -1) break;

        switch(c){
            case 0:
			    // for long args without short equivalent that just set a flag
			    // nothing left to do so just break.
			    if (long_options[option_index].flag != 0)
			    	break;
			    break;

            case 't':
                if (pulse_type != UNDEFINED){
                    printf("ERROR only one pulse type can be chosen\n");
                    return -1;
                }
                pulse_type = DSM2_22;
                break;

            case 'f':
                if (pulse_type != UNDEFINED){
                    printf("ERROR only one pulse type can be chosen\n");
                    return -1;
                }
                pulse_type = DSM2_11;
                break;

            case 's':
                if (pulse_type != UNDEFINED){
                    printf("ERROR only one pulse type can be chosen\n");
                    return -1;
                }
                pulse_type = DSMX_22;
                break;

            case 'd':
                en_debug = 1;
                printf("Enabling debug mode\n");
                break;

            case 'h':
                _print_usage();
                return -1;

            default:
                _print_usage();
                return -1;
        }
    }

    return 0;
}

// control gpio pins by specify which gpio to configure, its direction, and value to R/W based on direction
// when direction is set to output gpio_value is used to write, when set to input, gpio_value is set to read value
static int _gpio_control(unsigned int gpio_pin, int direction, unsigned int *gpio_value){
    int fd = -1;
    
    char read_buf[2] = {2};
    char write_buf[5] = {0};
    char gpio_file_name[128] = {0};

    // set write buf to pin number for export
    snprintf(write_buf, sizeof(write_buf), "%d", gpio_pin);

    // open export path
    fd = open(FILE_GPIO_EXPORT, O_WRONLY);
    if (fd < 0){
        printf("ERROR opening export file %s for pin %d\n", gpio_file_name, gpio_pin);
        return -1;
    }

    // export gpio pin
    write(fd, write_buf, sizeof(write_buf));
    close(fd);
    
    // debug prints
    if (en_debug) printf("Writing %s to %s for gpio pin %d\n", write_buf, FILE_GPIO_EXPORT, gpio_pin);

    // open
    snprintf(gpio_file_name, sizeof(gpio_file_name), FILE_GPIO_DIRECTION, gpio_pin);
    fd = open(gpio_file_name, O_RDWR);
    if (fd < 0){ 
        printf("ERROR opening direction file: %s\n", gpio_file_name);
        return -1;
    }

    memset(write_buf, 0, sizeof(write_buf)); // reset write buffer

    // set pin direction
    if (direction == GPIO_DIRECTION_INPUT){
        // set direction
        snprintf(write_buf, sizeof(write_buf), "%s", "in");
        write(fd, write_buf, sizeof(write_buf));
        close(fd);
        
        // debug prints
        if (en_debug) printf("Writing %s to %s for gpio pin %d\n", write_buf, FILE_GPIO_DIRECTION, gpio_pin);

        // open value file
        snprintf(gpio_file_name, sizeof(gpio_file_name), FILE_GPIO_VALUE, gpio_pin);
        fd = open(gpio_file_name, O_RDONLY);
        if (fd < 0){
            printf("ERROR failed to open value file: %s\n", gpio_file_name);
        }

        // read value from GPIO
        read(fd, read_buf, sizeof(read_buf));
        close(fd);

        sscanf(read_buf, "%d", gpio_value);

        // debug prints
        if (en_debug) printf("read %d from gpio pin %d\n", *gpio_value, gpio_pin);
    }
    else if(direction == GPIO_DIRECTION_OUTPUT){
        // set direction
        snprintf(write_buf, sizeof(write_buf), "%s", "out");
        write(fd, write_buf, sizeof(write_buf));
        close(fd);

        // open value file
        snprintf(gpio_file_name, sizeof(gpio_file_name), FILE_GPIO_VALUE, gpio_pin);
        fd = open(gpio_file_name, O_WRONLY);
        if (fd < 0){
            printf("ERROR failed to open value file: %s\n", gpio_file_name);
        }

        sprintf(write_buf, "%d", *gpio_value); // int to str
        write(fd, write_buf, sizeof(write_buf));
        close(fd);

        // debug prints
        if (en_debug) printf("Writing %s to %s for gpio pin %d\n", write_buf, FILE_GPIO_VALUE, gpio_pin);
    }
    else{
        printf("ERROR pin direction is not supported\n");
    }

    return 0;
}

// Spektrum binding blocking protocol. Puts receiver in binding mode
static int _spektrum_bind_blocking(){
    // init spektrum power
    unsigned int gpio_value = PIN_HIGH;
    _gpio_control(M0054_SPEKTRUM_POWER_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);

    // power off spektrum
    gpio_value = PIN_LOW;
    _gpio_control(M0054_SPEKTRUM_POWER_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
    usleep(100000); // wait to power down

    // spektrum power on
    gpio_value = PIN_HIGH;
    _gpio_control(M0054_SPEKTRUM_POWER_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);

    // init bind pin
    gpio_value = !PIN_HIGH; // inverted
    _gpio_control(M0054_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
    _gpio_control(M0154_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);

    usleep(50000);  //wait for Spektrum receiver to start up - reliably works up to 200ms

    // start sending pulses
    // set bind pin high
    gpio_value = !PIN_HIGH; // inverted
    _gpio_control(M0054_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
    _gpio_control(M0154_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
    usleep(60000);

    // 3 Pulses = DSM2 1024/22ms
    // 5 Pulses = DSM2 2048/11ms
    // 7 Pulses = DSMX 22ms
    // 9 Pulses = DSMX 11ms
    int n_pulses = pulse_type;
    if (en_debug) printf("number of pulses: %d\n", n_pulses);

    for(int i = 0; i < n_pulses; i++){
        // set bind pin low
        gpio_value = !PIN_LOW; // inverted
        _gpio_control(M0054_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
        _gpio_control(M0154_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
        usleep(116-2);

        // set bind pin high
        gpio_value = !PIN_HIGH; // inverted
        _gpio_control(M0054_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
        _gpio_control(M0154_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_OUTPUT, &gpio_value);
        usleep(116-2);
    }

    // set pin to input
    gpio_value = !PIN_HIGH; // inverted
    _gpio_control(M0054_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_INPUT, &gpio_value);
    _gpio_control(M0154_SPEKTRUM_BIND_PIN, GPIO_DIRECTION_INPUT, &gpio_value);

    return 0;
}

int main(int argc, char *argv[]){
    // parse arguments
    if (_parse_opts(argc, argv)) return -1;

    if (pulse_type == UNDEFINED){
        printf("ERROR a bind type must be chosen\n\n");
        _print_usage();
        return -1;
    }

    if(_spektrum_bind_blocking()){
        printf("ERROR failed to start bind\n");
        return -1;
    }
    else {
        printf("An orange light should now be flashing on your receiver\n");
        printf("If so, your receiver is successfully in binding mode\n");
    }

}
