# voxl-bind

## Summary

Utility for QRB5165 based hardware (VOXL2, RB5 Flight) that triggers a Spektrum radio to enter 'bind' mode.

## Command Line Options

```bash
-d, --debug         enable debug messages
-h, --help          print this help messsage
-t, --DSM2_22       3 pulses (DSM2 1024/22ms)
-f, --DSM2_11       5 pulses (DSM2 2048/11ms)
-s, --DSMX_22       7 pulses (DSMX 22ms)
```

## Hardware

The `voxl-bind` utility is used to toggle the power line going to the Spektrum RC port (GPIO 159) and activate a transistor (using GPIO 46) to pull the RX line down using the `M0094` board (internal to Sentinel / RB5 Flight drones).

This is done because the QRB5165 SoC can't toggle between a UART RX ping and a GPIO output pin at runtime....

![M0094](docs/M0094-M0054.png)
